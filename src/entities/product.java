package entities;

public class product {

	private int id;
	private String libelle;
	private String description;
	private Double prix;
	
	
	public product(int id, String libelle, String description, Double prix) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.description = description;
		this.prix = prix;
	}
	
	public product() {
		super();
	
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Double getPrix() {
		return prix;
	}


	public void setPrix(Double prix) {
		this.prix = prix;
	}


	@Override
	public String toString() {
		return "product [id=" + id + ", libelle=" + libelle + ", description=" + description + ", prix=" + prix + "]";
	}
	
	
	
	
	
	
	
}
