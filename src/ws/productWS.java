package ws;


import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import entities.product;

@WebService
public interface productWS {
	
	public product findProduct(int id);
	
	@WebMethod
	public List<product> findAll();

}


	
	

