package ws;
import javax.jws.*;

@WebService(endpointInterface = "ws.Demo")
public class DemoImpl implements Demo{
	@Override
	public String helloworld() {
		
		return "Hello world";
	}
	
	@Override
	public String hi(String nom) {
		String hi = "Hi "+nom;
		return hi;
		
	}
}
