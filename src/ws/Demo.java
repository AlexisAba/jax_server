package ws;

import javax.jws.*;

@WebService
public interface Demo {
	
	public String helloworld();
	
	@WebMethod
	public String hi(String fullName);

}
