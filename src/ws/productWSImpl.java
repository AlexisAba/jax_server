package ws;

import java.util.List;

import javax.jws.WebService;

import dao.daoProduct;
import entities.product;

@WebService(endpointInterface = "ws.productWS")
public class productWSImpl implements productWS{
	
	private daoProduct dao = new daoProduct();
	@Override
	public product findProduct(int id){
		
		return this.dao.findProduct(id);
	}
	
	@Override
	public List<product> findAll()  {
		return this.dao.findAll();
		
	}
}
